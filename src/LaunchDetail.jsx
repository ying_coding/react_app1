import ImmutablePropTypes from 'react-immutable-proptypes';
import Badge from 'react-bootstrap/Badge';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

/**
 * Shows detail information of a launch
 */
export default function LaunchDetail({ launch, ...others }) {
  // calculates which status badge should be displayed
  const status = launch.get('upcoming') ? (
    <Badge variant="info">Upcoming</Badge>
  ) : launch.get('success') ? (
    <Badge variant="success">Success</Badge>
  ) : (
    <Badge variant="danger">Failures: {launch.get('failures').count()}</Badge>
  );

  // just shows some information
  return (
    <Form {...others}>
      <Form.Group as={Row} controlId="launchStatus">
        <Form.Label column sm="2">
          Status
        </Form.Label>
        <Col sm="10" className="launch__status">
          {status}
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="launchId">
        <Form.Label column sm="2">
          ID
        </Form.Label>
        <Col sm="10">
          <Form.Control plaintext readOnly defaultValue={launch.get('id')} />
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="launchDate">
        <Form.Label column sm="2">
          Date
        </Form.Label>
        <Col sm="10">
          <Form.Control
            plaintext
            readOnly
            defaultValue={launch.get('date_utc')}
          />
        </Col>
      </Form.Group>
      <Form.Group as={Row} controlId="launchFlight">
        <Form.Label column sm="2">
          Flight Number
        </Form.Label>
        <Col sm="10">
          <Form.Control
            plaintext
            readOnly
            defaultValue={launch.get('flight_number')}
          />
        </Col>
      </Form.Group>
    </Form>
  );
}

LaunchDetail.propTypes = {
  launch: ImmutablePropTypes.map.isRequired,
};
