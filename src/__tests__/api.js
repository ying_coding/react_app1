import * as API from '../api';

describe('Tests of data loading APIs', () => {
  const fetchMock =
    (result, isOk = true) =>
    () =>
      Promise.resolve({
        ok: isOk,
        json: () => Promise.resolve(result),
      });

  // mock fetch API to test without sending real requests
  global.fetch = jest.fn();

  beforeEach(() => {
    fetch.mockClear();
  });

  it('should fetch and return result', async () => {
    const mockData = { someData: 1 };
    const url = 'dummy';
    const options = { header: [] };
    fetch.mockImplementation(fetchMock(mockData, true));
    const result = await API.fetchAndCheck(url, options);
    expect(result).toEqual(mockData);
  });

  it('should report connection error', async () => {
    const url = 'dummy';
    const options = { header: [] };
    const reason = 'offline';
    fetch.mockImplementationOnce(() => Promise.reject(reason));
    try {
      const result = await API.fetchAndCheck(url, options);
    } catch (error) {
      expect(error).toEqual(reason);
    }
  });

  it('should report http error', async () => {
    const url = 'dummy';
    const options = { header: [] };
    const reason = 'internal error';
    fetch.mockImplementationOnce(() =>
      Promise.resolve({
        ok: false,
        statusText: reason,
      })
    );
    try {
      const result = await API.fetchAndCheck(url, options);
    } catch (error) {
      expect(error).toEqual(new Error(reason));
    }
  });

  it('should get JSON', async () => {
    const mockData = { someData: 1 };
    const url = 'dummy';
    fetch.mockImplementation(fetchMock(mockData, true));
    const result = await API.getJSON(url);
    expect(result).toEqual(mockData);
    expect(fetch.mock.calls[0][1].method).toBe('GET');
  });

  it('should post JSON', async () => {
    const mockData = { someData: 1 };
    const url = 'dummy';
    const postData = { someData: 2 };
    fetch.mockImplementation(fetchMock(mockData, true));
    const result = await API.postJSON(url, postData);
    expect(result).toEqual(mockData);
    expect(fetch.mock.calls[0][1].method).toBe('POST');
    expect(fetch.mock.calls[0][1].body).toBe(JSON.stringify(postData));
  });

  it('should load launch data', async () => {
    const mockData = { someData: 1 };
    const id = 'dummy';
    fetch.mockImplementation(fetchMock(mockData, true));
    const result = await API.loadLaunch(id);
    expect(fetch.mock.calls[0][0]).toContain(id);
    expect(result).toEqual(mockData);
  });

  it('should query launches', async () => {
    const mockData = { docs: [1, 2, 3] };
    fetch.mockImplementation(fetchMock(mockData, true));
    const result = await API.queryLaunches();
    expect(result).toEqual(mockData.docs);
  });

  it('should load rocket data', async () => {
    const mockData = { someData: 1 };
    const id = 'dummy';
    fetch.mockImplementation(fetchMock(mockData, true));
    const result = await API.loadRocket(id);
    expect(fetch.mock.calls[0][0]).toContain(id);
    expect(result).toEqual(mockData);
  });

  it('should query launches of a rocket', async () => {
    const mockData = { totalDocs: 999 };
    fetch.mockImplementation(fetchMock(mockData, true));
    const result = await API.loadRocketLaunches();
    expect(result).toEqual(mockData.totalDocs);
  });
});

describe('Tests of logger APIs', () => {
  console.log = jest.fn();
  it('should log the given messages', () => {
    const msgs = ['msg1', 'msg2'];
    API.logMessage(...msgs);
    expect(console.log.mock.calls[0]).toContain(msgs[0]);
    expect(console.log.mock.calls[0]).toContain(msgs[1]);
  });
});
