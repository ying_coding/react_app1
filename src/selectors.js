export const getLaunches = (state) => state.launches;

export const getObject = (id) => (state) => state.objectById.get(id);
