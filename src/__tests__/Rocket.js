import Immutable from 'immutable';
import { render, screen, dispatcher } from '../testUtils';
import Rocket from '../Rocket';
import { loadRocket } from '../actions';

describe('test Rocket component', () => {
  const emptyState = Immutable.Map();

  it('renders Spinner and triggers loading initially', () => {
    const id = 'dummy';
    const initialState = {
      objectById: emptyState,
    };
    render(<Rocket rocketId={id} />, { initialState });
    const target = screen.getByTestId('rocket_spinner');
    expect(target).toBeInTheDocument();
    expect(dispatcher).toBeCalledWith(loadRocket(id));
  });

  it('renders ErrorState if no data fetched', () => {
    const id = 'dummy';
    const initialState = {
      objectById: emptyState.set(id, null),
    };
    render(<Rocket rocketId={id} />, { initialState });
    const target = screen.getByTestId('rocket_error');
    expect(target).toBeInTheDocument();
    expect(dispatcher).not.toHaveBeenCalled();
  });

  it('renders detail if data exists', () => {
    const id = 'dummy';
    const name = 'bar';
    const description = 'foo';
    const dummy = {
      name,
      description,
    };
    const initialState = {
      objectById: emptyState.set(id, Immutable.fromJS(dummy)),
    };
    render(<Rocket rocketId={id} />, { initialState });
    expect(screen.getByDisplayValue(name)).toBeInTheDocument();
    expect(screen.getByDisplayValue(description)).toBeInTheDocument();
    expect(dispatcher).not.toHaveBeenCalled();
  });
});
