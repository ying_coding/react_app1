import Immutable from 'immutable';
import { render, screen, dispatcher, fireEvent } from '../testUtils';
import Launches from '../Launches';
import { queryLaunches } from '../actions';

describe('test Launches component', () => {
  const emptyState = Immutable.List();

  it('renders Spinner and triggers loading initially', () => {
    render(<Launches />);
    const target = screen.getByTestId('launches_spinner');
    expect(target).toBeInTheDocument();
    expect(dispatcher).toBeCalledWith(queryLaunches());
  });

  it('renders ErrorState if no data fetched', () => {
    const initialState = {
      launches: emptyState,
    };
    render(<Launches />, { initialState });
    const target = screen.getByTestId('launches_error');
    expect(target).toBeInTheDocument();
  });

  it('renders launces if data exists', () => {
    const launches = [
      {
        id: '#001',
        upcoming: false,
      },
      {
        id: '#002',
        upcoming: false,
      },
      {
        id: '#003',
        upcoming: false,
      },
      {
        id: '#004',
        upcoming: true,
      },
    ];
    const initialState = {
      launches: Immutable.fromJS(launches),
    };
    render(<Launches />, { initialState });
    expect(screen.queryByText('#001')).not.toBeInTheDocument();
    expect(screen.getByTestId('#002')).toBeInTheDocument();
    expect(screen.getByTestId('#003')).toBeInTheDocument();
    expect(screen.getByTestId('#004')).toBeInTheDocument();
  });

  it('reacts to carousel navigation', async () => {
    const launches = [
      {
        id: '#001',
        upcoming: false,
      },
      {
        id: '#002',
        upcoming: false,
      },
      {
        id: '#003',
        upcoming: false,
      },
      {
        id: '#004',
        upcoming: true,
      },
    ];
    const initialState = {
      launches: Immutable.fromJS(launches),
    };
    const { container } = render(<Launches />, { initialState });
    expect(screen.queryByText('#001')).not.toBeInTheDocument();
    expect(screen.getByTestId('#002')).toBeInTheDocument();
    expect(screen.getByTestId('#003')).toBeInTheDocument();
    expect(screen.getByTestId('#004')).toBeInTheDocument();
    const button = container.querySelector('.carousel-control-prev');
    expect(button).toBeInTheDocument();
    await fireEvent.click(button);
    expect(screen.getByTestId('#001')).toBeInTheDocument();
  });
});
