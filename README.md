# Demo Application To Show Some Insights About SpaceX Launches

This project will demostration retrieving and showing information from another API, in this case, 
data of SpaceX launches.

Visit the demo application here: [https://ying_coding.gitlab.io/react_app1/](https://ying_coding.gitlab.io/react_app1/).
The latest launch will be showed as a card in the middle of a carousel after the data get loaded. You can switch the detail 
area to show information about the rocket used for that launch by clicking `Rocket` tab. You can also use the buttons
on the left and right side, which are showed as arrows, to navigate to previous or next launch. 


## Data Source

The data would be fetched using SpaceX API: 
[https://github.com/r-spacex/SpaceX-API/tree/master/docs/v4](https://github.com/r-spacex/SpaceX-API/tree/master/docs/v4)

### CORS

The data source server supports CORS (Cross-Origin Resource Sharing) protocol. The browser will use preflight request to 
confirm that, therefore the application can fetch data from that server. 


## Libraries

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

The application is implemented in [React](https://reactjs.org/).

To keep `separation of concerns`, the react components would only handle UI stuff. The data loading
logic would be done in `redux` way and written in `sagas` by using [redux-saga](https://redux-saga.js.org).
If you want to change to e.g. another version of the data source, you can change the constants and API usages, without
almost touching the UI components, if the basic data schema not changed.

The data saved in store would be kept as [Immutable](https://immutable-js.github.io/immutable-js/docs/#/) data, to allow easy
shallow equal comparison and avoid unnecessary component rerenderings.

To enhance the user experience, the component library [React Bootstrap](https://react-bootstrap.github.io/) is also get used.

The stylesheets get written in `Sass`(`.scss`) to use e.g. variables and nested rules, and to be easy customized.

Tests would be done using [Jest](https://jestjs.io/).


## How To Start the App / Available Scripts

To run the application locally, ensure that you have [npm](https://www.npmjs.com/get-npm) installed.
Clone the project repository and in the project directory, you must run:

### `npm install`

Installs the dependencies for the application and sets the developing/building environement up.

After that, you can run following available scripts:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Other Resources

The rocket icons are licensed by [IconsMind](http://www.iconsmind.com)

## Disclaimer
The names SpaceX as well as related names, marks, emblems and images are registered trademarks of their respective owners.

## License

MIT