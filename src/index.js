import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';

import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';

import reducer from './reducers';
import appSaga from './sagas';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();

// enhance store to allow debugging using redux devtools
const basicEnhancer = applyMiddleware(sagaMiddleware);
const storeEnhancer =
  window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ !== undefined
    ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({ name: 'global' })(
        basicEnhancer
      )
    : basicEnhancer;

// mount it on the Store
const store = createStore(reducer, storeEnhancer);

// then run the saga
sagaMiddleware.run(appSaga);

// mount the app
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);
