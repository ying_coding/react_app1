import { ACTIONS } from './constants';

export function loadLaunch(id) {
  return {
    type: ACTIONS.LOAD_LAUNCH,
    payload: { id },
  };
}

export function queryLaunches() {
  return {
    type: ACTIONS.QUERY_LAUNCHES,
  };
}

export function loadRocket(id) {
  return {
    type: ACTIONS.LOAD_ROCKET,
    payload: { id },
  };
}
