import PropTypes from 'prop-types';
import Button from 'react-bootstrap/Button';

/**
 * Showing fetch error message
 * `others` grants passing e.g. data-testid down
 */
export default function ErrorState({ retry, ...others }) {
  return (
    <div className="error-state" {...others}>
      <p>No data fetched</p>
      <p>Please try later or report the issue.</p>
      {retry && (
        <Button variant="primary" onClick={retry}>
          Retry
        </Button>
      )}
    </div>
  );
}

ErrorState.propTypes = {
  retry: PropTypes.func,
};
