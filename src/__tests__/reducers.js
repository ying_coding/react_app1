import Immutable from 'immutable';
import reducer from '../reducers';
import { ACTIONS } from '../constants';

describe('Tests of (combined) reducers', () => {
  it('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual({
      launches: null,
      objectById: Immutable.Map(),
    });
  });

  it('should receive launches', () => {
    const action = {
      type: ACTIONS.RECEIVE_LAUNCHES,
      payload: [1, 2, 3],
    };
    expect(reducer(undefined, action).launches).toEqual(
      Immutable.fromJS(action.payload)
    );
  });

  it('should receive object', () => {
    const action = {
      type: ACTIONS.RECEIVE_OBJECT,
      meta: { id: 'dummy' },
      payload: { someData: '1' },
    };
    expect(reducer(undefined, action).objectById.get(action.meta.id)).toEqual(
      Immutable.fromJS(action.payload)
    );
  });
});
