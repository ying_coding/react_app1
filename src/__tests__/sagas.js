import { runSaga } from 'redux-saga';
import { ACTIONS } from '../constants';
import * as API from '../api';
import {
  loadLaunch,
  queryLaunches,
  loadRocket,
  loadRocketLaunches,
} from '../sagas';

async function recordSaga(saga, initialAction) {
  const dispatched = [1];

  const result = await runSaga(
    {
      dispatch: (action) => dispatched.push(action),
    },
    saga,
    initialAction
  ).toPromise();

  return dispatched;
}

describe('Tests of sagas', () => {
  API.loadLaunch = jest.fn();
  API.queryLaunches = jest.fn();
  API.loadRocket = jest.fn();
  API.loadRocketLaunches = jest.fn();
  // disable log messages to console during testing
  API.logMessage = jest.fn();

  beforeEach(() => {
    jest.resetAllMocks();
  });

  const mockData = { someData: 1 };

  it('should query all launches', async () => {
    const initAction = {};
    API.queryLaunches.mockImplementation(() => mockData);

    const dispatched = await recordSaga(queryLaunches, initAction);

    expect(API.queryLaunches).toHaveBeenCalled();
    expect(dispatched).toContainEqual({
      type: ACTIONS.RECEIVE_LAUNCHES,
      payload: mockData,
    });
  });

  it('should handle error by querying launches', async () => {
    const initAction = {};
    API.queryLaunches.mockImplementation(() => {
      throw new Error();
    });

    const dispatched = await recordSaga(queryLaunches, initAction);

    expect(API.queryLaunches).toHaveBeenCalled();
    expect(dispatched).toContainEqual({
      type: ACTIONS.RECEIVE_LAUNCHES,
      payload: null,
    });
  });

  it('should load launch by id', async () => {
    const initAction = { payload: { id: 'dummy' } };
    API.loadLaunch.mockImplementation(() => mockData);

    const dispatched = await recordSaga(loadLaunch, initAction);

    expect(API.loadLaunch).toHaveBeenCalledWith(initAction.payload.id);
    expect(dispatched).toContainEqual({
      type: ACTIONS.RECEIVE_OBJECT,
      meta: { id: initAction.payload.id },
      payload: mockData,
    });
  });

  it('should handle error by loading launch by id', async () => {
    const initAction = { payload: { id: 'dummy' } };
    API.loadLaunch.mockImplementation(() => {
      throw new Error();
    });

    const dispatched = await recordSaga(loadLaunch, initAction);

    expect(API.loadLaunch).toHaveBeenCalledWith(initAction.payload.id);
    expect(dispatched).toContainEqual({
      type: ACTIONS.RECEIVE_OBJECT,
      meta: { id: initAction.payload.id },
      payload: null,
    });
  });

  it('should load rocket by id', async () => {
    const initAction = { payload: { id: 'dummy' } };
    API.loadRocket.mockImplementation(() => mockData);
    API.loadRocketLaunches.mockImplementation(() => 999);
    const expectedData = { ...mockData, launchCounts: 999 };

    const dispatched = await recordSaga(loadRocket, initAction);

    expect(API.loadRocket).toHaveBeenCalledWith(initAction.payload.id);
    expect(API.loadRocketLaunches).toHaveBeenCalledWith(initAction.payload.id);
    expect(dispatched).toContainEqual({
      type: ACTIONS.RECEIVE_OBJECT,
      meta: { id: initAction.payload.id },
      payload: expectedData,
    });
  });

  it('should handle error by loading rocket by id', async () => {
    const initAction = { payload: { id: 'dummy' } };
    API.loadRocket.mockImplementation(() => {
      throw new Error();
    });
    const dispatched = await recordSaga(loadRocket, initAction);

    expect(API.loadRocket).toHaveBeenCalledWith(initAction.payload.id);
    expect(API.loadRocketLaunches.mock.calls.length).toEqual(0);
    expect(dispatched).toContainEqual({
      type: ACTIONS.RECEIVE_OBJECT,
      meta: { id: initAction.payload.id },
      payload: null,
    });
  });
  it('should handle error by loading rocket launches by id', async () => {
    const initAction = { payload: { id: 'dummy' } };
    API.loadRocket.mockImplementation(() => mockData);
    API.loadRocketLaunches.mockImplementation(() => {
      throw new Error();
    });
    const dispatched = await recordSaga(loadRocket, initAction);

    expect(API.loadRocket).toHaveBeenCalledWith(initAction.payload.id);
    expect(API.loadRocketLaunches).toHaveBeenCalledWith(initAction.payload.id);
    expect(dispatched).toContainEqual({
      type: ACTIONS.RECEIVE_OBJECT,
      meta: { id: initAction.payload.id },
      payload: null,
    });
  });
});
