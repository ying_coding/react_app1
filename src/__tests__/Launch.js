import Immutable from 'immutable';
import { render, screen, dispatcher } from '../testUtils';
import Launch from '../Launch';
import { loadLaunch } from '../actions';

describe('test Launch component', () => {
  const emptyState = Immutable.Map();

  it('renders Spinner and triggers loading initially', () => {
    const id = 'dummy';
    const initialState = {
      objectById: emptyState,
    };
    render(<Launch launchId={id} index={0} totalIndex={1} />, { initialState });
    const target = screen.getByTestId('launch_spinner');
    expect(target).toBeInTheDocument();
    expect(dispatcher).toBeCalledWith(loadLaunch(id));
  });

  it('renders ErrorState if no data fetched', () => {
    const id = 'dummy';
    const initialState = {
      objectById: emptyState.set(id, null),
    };
    render(<Launch launchId={id} index={0} totalIndex={1} />, { initialState });
    const target = screen.getByTestId('launch_error');
    expect(target).toBeInTheDocument();
    expect(dispatcher).not.toHaveBeenCalled();
  });

  it('renders detail if data exists', () => {
    const id = 'dummy';
    const name = 'bar';
    const details = 'foo';
    const dummy = {
      name,
      details,
      failures: [],
    };
    const initialState = {
      objectById: emptyState.set(id, Immutable.fromJS(dummy)),
    };
    render(<Launch launchId={id} index={0} totalIndex={1} />, { initialState });
    expect(screen.getByText(name)).toBeInTheDocument();
    expect(screen.getByText(details)).toBeInTheDocument();
    expect(dispatcher).not.toHaveBeenCalled();
  });

  it('renders indexes', () => {
    const id = 'dummy';
    const name = 'bar';
    const details = 'foo';
    const dummy = {
      name,
      details,
      failures: [],
    };
    const initialState = {
      objectById: emptyState.set(id, Immutable.fromJS(dummy)),
    };
    render(<Launch launchId={id} index={42} totalIndex={43} />, {
      initialState,
    });
    expect(screen.getByText(/42/i)).toBeInTheDocument();
    expect(screen.getByText(/43/i)).toBeInTheDocument();
  });
});
