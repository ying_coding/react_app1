import { render, screen } from '../testUtils';
import App from '../App';

describe('test App component', () => {
  it('renders App', () => {
    render(<App />);
    const target = screen.getByText(/Overview of Launches/i);
    expect(target).toBeInTheDocument();
  });
});
