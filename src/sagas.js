import { ACTIONS } from './constants';
import { call, put, takeEvery } from 'redux-saga/effects';
import * as API from './api';

export function* loadLaunch(action) {
  let data = null;
  // calls corresponding API,
  // handles both connection and http error by logging it
  try {
    data = yield call(API.loadLaunch, action.payload.id);
  } catch (error) {
    API.logMessage('error', error);
  }
  yield put({
    type: ACTIONS.RECEIVE_OBJECT,
    meta: { id: action.payload.id },
    payload: data,
  });
}

export function* queryLaunches(action) {
  let data = null;
  try {
    data = yield call(API.queryLaunches);
  } catch (error) {
    API.logMessage('error', error);
  }
  yield put({
    type: ACTIONS.RECEIVE_LAUNCHES,
    payload: data,
  });
}

export function* loadRocket(action) {
  let data = null;
  try {
    data = yield call(API.loadRocket, action.payload.id);
    // extend rocket data with aggregation: launch counts
    data.launchCounts = yield call(API.loadRocketLaunches, action.payload.id);
  } catch (error) {
    API.logMessage('error', error);
    // reset data to allow retrying
    data = null;
  }
  yield put({
    type: ACTIONS.RECEIVE_OBJECT,
    meta: { id: action.payload.id },
    payload: data,
  });
}

export default function* watcher() {
  // watchs and handle every action
  yield takeEvery(ACTIONS.LOAD_LAUNCH, loadLaunch);
  yield takeEvery(ACTIONS.QUERY_LAUNCHES, queryLaunches);
  yield takeEvery(ACTIONS.LOAD_ROCKET, loadRocket);
}
