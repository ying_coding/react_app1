import Launches from './Launches';
import './App.scss';

function App() {
  return (
    <div className="app">
      <h4 className="title">Overview of Launches</h4>
      <Launches />
    </div>
  );
}

export default App;
