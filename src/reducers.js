import Immutable from 'immutable';
import { combineReducers } from 'redux';
import { ACTIONS } from './constants';

/**
 * Keep launches collection with meta data
 */
export function launches(state = null, action) {
  switch (action.type) {
    case ACTIONS.RECEIVE_LAUNCHES:
      // launches would be saved as Immutable List
      return Immutable.fromJS(action.payload);
    default:
      return state;
  }
}

/**
 * Keep object information by its ID
 */
export function objectById(state = Immutable.Map(), action) {
  switch (action.type) {
    case ACTIONS.RECEIVE_OBJECT:
      return state.set(action.meta.id, Immutable.fromJS(action.payload));
    default:
      return state;
  }
}

export default combineReducers({
  launches,
  objectById,
});
