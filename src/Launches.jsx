import { useEffect, useState, useCallback } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import Carousel from 'react-bootstrap/Carousel';
import { useDispatch, useSelector } from 'react-redux';
import { queryLaunches } from './actions';
import { getLaunches } from './selectors';
import Launch from './Launch';
import ErrorState from './ErrorState';

export default function Launches() {
  const dispatch = useDispatch();
  const launches = useSelector(getLaunches);
  const [index, setIndex] = useState(-1);

  // function to load data
  const loadData = useCallback(() => {
    dispatch(queryLaunches());
  }, [dispatch]);

  useEffect(() => {
    // loads data once by mounting
    loadData();
  }, [loadData]);

  // show the latest launch initially
  useEffect(() => {
    if (index === -1 && launches !== null) {
      const target = launches.findLastIndex((item) => !!!item.get('upcoming'));
      setIndex(target);
    }
  }, [index, launches]);

  if (launches === null) {
    // loading data
    return (
      <Spinner
        data-testid="launches_spinner"
        animation="border"
        variant="secondary"
      />
    );
  }

  // optimized: calculates `viewport` - just shows 3
  // items (previous, current, next one) to avoid rendering
  // too many elements
  const total = launches.count();
  const begin = Math.max(0, index - 1);
  const activeIndex = index - begin;
  const end = index + 1;
  const showing = launches.slice(begin, end + 1);

  // recalculate to real index
  const handleSelect = (selectedIndex) => {
    setIndex(index + selectedIndex - activeIndex);
  };

  if (index < 0) {
    // no data loaded, shows empty state
    return <ErrorState data-testid="launches_error" retry={loadData} />;
  }

  return (
    <div>
      <Carousel
        className="launches"
        activeIndex={activeIndex}
        indicators={false}
        interval={null}
        onSelect={handleSelect}
        fade={true}
      >
        {showing.map((launch) => {
          const launchId = launch.get('id');
          return (
            <Carousel.Item data-testid={launchId} key={launchId}>
              <Launch launchId={launchId} index={index} totalIndex={total} />
            </Carousel.Item>
          );
        })}
      </Carousel>
    </div>
  );
}
