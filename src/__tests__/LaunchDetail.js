import Immutable from 'immutable';
import { render, screen } from '../testUtils';
import LaunchDetail from '../LaunchDetail';

describe('test LaunchDetail component', () => {
  it('renders upcoming launch', () => {
    const id = 'dummy';
    const name = 'bar';
    const details = 'foo';
    const dummy = {
      id,
      name,
      details,
      upcoming: true,
      failures: [],
    };
    render(<LaunchDetail launch={Immutable.fromJS(dummy)} />);
    expect(screen.getByDisplayValue(id)).toBeInTheDocument();
    expect(screen.getByText(/upcoming/i)).toBeInTheDocument();
  });

  it('renders successful launch', () => {
    const id = 'dummy';
    const name = 'bar';
    const details = 'foo';
    const dummy = {
      id,
      name,
      details,
      upcoming: false,
      success: true,
      failures: [],
    };
    render(<LaunchDetail launch={Immutable.fromJS(dummy)} />);
    expect(screen.getByDisplayValue(id)).toBeInTheDocument();
    expect(screen.getByText(/success/i)).toBeInTheDocument();
  });

  it('renders failed launch', () => {
    const id = 'dummy';
    const name = 'bar';
    const details = 'foo';
    const dummy = {
      id,
      name,
      details,
      upcoming: false,
      success: false,
      failures: [1, 2],
    };
    render(<LaunchDetail launch={Immutable.fromJS(dummy)} />);
    expect(screen.getByDisplayValue(id)).toBeInTheDocument();
    expect(screen.getByText(/failure/i)).toBeInTheDocument();
  });
});
