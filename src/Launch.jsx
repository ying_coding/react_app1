import { useEffect, useState, useCallback, Fragment } from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';
import Card from 'react-bootstrap/Card';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import { useDispatch, useSelector } from 'react-redux';
import { loadLaunch } from './actions';
import { getObject } from './selectors';
import RocketIcon from './rocket-icon-128.png';
import LaunchDetail from './LaunchDetail';
import Rocket from './Rocket';
import ErrorState from './ErrorState';

/**
 * Shows single launch, triggers data fetching
 */
export default function Launch({ launchId, index, totalIndex }) {
  const dispatch = useDispatch();
  const launch = useSelector(getObject(launchId));

  // function to trigger loading data
  const loadData = useCallback(() => {
    dispatch(loadLaunch(launchId));
  }, [dispatch, launchId]);

  useEffect(() => {
    if (launch === undefined) {
      // Data not fetched yet, do it just once
      loadData();
    }
  }, [loadData, launch]);

  const [tabKey, setTabKey] = useState('detail');

  let content;
  if (launch === undefined) {
    // data in fetching, waiting
    content = (
      <Spinner
        data-testid="launch_spinner"
        animation="border"
        variant="secondary"
      />
    );
  } else if (launch === null) {
    // fetching finished without data, showing empty state
    content = <ErrorState data-testid="launch_error" retry={loadData} />;
  } else {
    // data exists
    const imgSrc =
      launch.getIn(['links', 'flickr', 'original', 0]) ||
      launch.getIn(['patch', 'small']) ||
      RocketIcon;

    content = (
      <Fragment>
        <div className="launch__intro">
          <div className="launch__thumbnail">
            <img src={imgSrc} alt="thumbnail" />
          </div>
          <div className="launch__desc">{launch.get('details')}</div>
        </div>
        <Tabs activeKey={tabKey} onSelect={setTabKey}>
          <Tab eventKey="detail" title="Detail">
            <LaunchDetail data-testid="launch_detail" launch={launch} />
          </Tab>
          <Tab eventKey="rocket" title="Rocket">
            {tabKey === 'rocket' ? (
              <Rocket
                data-testid="launch_rocket"
                rocketId={launch.get('rocket')}
              />
            ) : null}
          </Tab>
        </Tabs>
      </Fragment>
    );
  }

  return (
    <div className="launch">
      <Card>
        <Card.Header className="launch__header">
          <h5 className="launch__name">{launch && launch.get('name')}</h5>
          <h6>
            {index} / {totalIndex}
          </h6>
        </Card.Header>
        <Card.Body>{content}</Card.Body>
      </Card>
    </div>
  );
}

Launch.propTypes = {
  launchId: PropTypes.string.isRequired,
  index: PropTypes.number,
  totalIndex: PropTypes.number,
};
