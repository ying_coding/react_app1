import Immutable from 'immutable';
import * as selectors from '../selectors';

describe('Tests of selectors', () => {
  it('should select launches', () => {
    const state = {
      launches: [1, 2, 3],
    };
    expect(selectors.getLaunches(state)).toEqual(state.launches);
  });

  it('should select object by id', () => {
    const id = 'dummy';
    const obj = { someData: 1 };
    const state = {
      objectById: Immutable.Map([[id, obj]]),
    };
    expect(selectors.getObject(id)(state)).toEqual(obj);
  });
});
