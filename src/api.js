import { dataAPIRoot } from './constants';

/**
 * Wraps the standard fetch api with response status check,
 * throws error for non-connection error, e.g. status 400 - 500.
 * Also use async/await to format code using proimises
 * straight forward.
 *
 * @param {string} url target url to fetch data
 * @param {Object} options options passed to fetch api
 * @returns JSON result for usage in this app
 */
export async function fetchAndCheck(url, options) {
  const response = await fetch(url, options);
  // Check for e.g. 400-500 errors
  if (!response.ok) {
    throw new Error(response.statusText);
  }
  const result = await response.json();
  return result;
}

/**
 *  Wrapper for fetch JSON data using GET
 */
export async function getJSON(url) {
  // Setting 'Content-Type': 'application/json' to
  // activate CORS using preflight
  const options = {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const result = await fetchAndCheck(url, options);
  return result;
}

/**
 *  Wrapper for fetch JSON data using POST
 *
 * @param {string} url target url to fetch data
 * @param {Object} data content that should be posted to server
 * @returns JSON result from the server
 */
export async function postJSON(url, data) {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  };
  const result = await fetchAndCheck(url, options);
  return result;
}

/**
 * Load single launch by id
 */
export async function loadLaunch(id) {
  const result = await getJSON(`${dataAPIRoot}/launches/${id}`);
  return result;
}

/**
 * Query all launches
 */
export async function queryLaunches() {
  const query = {
    query: {},
    options: {
      select: 'id date_utc upcoming',
      sort: 'date_utc',
      pagination: false,
    },
  };
  const result = await postJSON(`${dataAPIRoot}/launches/query`, query);
  return result.docs;
}

/**
 * Load single launch by id
 */
export async function loadRocket(id) {
  const result = await getJSON(`${dataAPIRoot}/rockets/${id}`);
  return result;
}

/**
 * Query all launches of one rocket by its id - to do the aggregation
 */
export async function loadRocketLaunches(id) {
  const query = {
    query: { rocket: id },
    options: {
      select: 'id',
      limit: 1,
    },
  };
  const result = await postJSON(`${dataAPIRoot}/launches/query`, query);
  return result.totalDocs;
}

/**
 * Warpper for logger, can be changed e.g. for testing
 */
export function logMessage(...msgs) {
  console.log('[logger]: ', ...msgs);
}
