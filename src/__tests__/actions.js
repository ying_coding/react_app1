import * as actionCreators from '../actions';
import { ACTIONS } from '../constants';

describe('Tests of actions', () => {
  it('should trigger loading launch', () => {
    const id = 'dummy';
    const expectedAction = {
      type: ACTIONS.LOAD_LAUNCH,
      payload: { id },
    };
    expect(actionCreators.loadLaunch(id)).toEqual(expectedAction);
  });

  it('should trigger querying launches', () => {
    const expectedAction = {
      type: ACTIONS.QUERY_LAUNCHES,
    };
    expect(actionCreators.queryLaunches()).toEqual(expectedAction);
  });

  it('should trigger loading rocket', () => {
    const id = 'dummy';
    const expectedAction = {
      type: ACTIONS.LOAD_ROCKET,
      payload: { id },
    };
    expect(actionCreators.loadRocket(id)).toEqual(expectedAction);
  });
});
