import ReactDOM from 'react-dom';

// mock the real render
jest.mock('react-dom', () => ({ render: jest.fn() }));

describe('test index.js', () => {
  it('renders without crashing', () => {
    const div = document.createElement('div');
    div.id = 'root';
    document.body.appendChild(div);
    require('../index.js');
    expect(ReactDOM.render.mock.calls[0][1]).toBe(div);
  });
});
