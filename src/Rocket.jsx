import { useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';
import Spinner from 'react-bootstrap/Spinner';
import Form from 'react-bootstrap/Form';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useDispatch, useSelector } from 'react-redux';
import { loadRocket } from './actions';
import { getObject } from './selectors';
import ErrorState from './ErrorState';

export default function Rocket({ rocketId }) {
  const dispatch = useDispatch();
  const rocket = useSelector(getObject(rocketId));

  // function to trigger loading data
  const loadData = useCallback(() => {
    dispatch(loadRocket(rocketId));
  }, [dispatch, rocketId]);

  useEffect(() => {
    if (rocket === undefined) {
      // Data not fetched yet, do it just once
      loadData();
    }
  }, [loadData, rocket]);

  if (rocket === undefined) {
    // fetching data
    return (
      <Spinner
        data-testid="rocket_spinner"
        animation="border"
        variant="secondary"
      />
    );
  } else if (rocket === null) {
    // no data loaded, shows empty state
    return <ErrorState data-testid="rocket_error" retry={loadData} />;
  } else {
    // show some information
    return (
      <Form>
        <Form.Group as={Row} controlId="rocketName">
          <Form.Label column sm="2">
            Name
          </Form.Label>
          <Col sm="10">
            <Form.Control
              plaintext
              readOnly
              defaultValue={rocket.get('name')}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="rocketId">
          <Form.Label column sm="2">
            ID
          </Form.Label>
          <Col sm="10">
            <Form.Control plaintext readOnly defaultValue={rocket.get('id')} />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="rocketLaunches">
          <Form.Label column sm="2">
            Total Launches
          </Form.Label>
          <Col sm="10">
            <Form.Control
              plaintext
              readOnly
              defaultValue={rocket.get('launchCounts')}
            />
          </Col>
        </Form.Group>
        <Form.Group as={Row} controlId="rocketLaunches">
          <Form.Label column sm="2">
            Description
          </Form.Label>
          <Col sm="10">
            <Form.Control
              as="textarea"
              plaintext
              readOnly
              defaultValue={rocket.get('description')}
            />
          </Col>
        </Form.Group>
      </Form>
    );
  }
}

Rocket.propTypes = {
  rocketId: PropTypes.string.isRequired,
};
