import { render, screen, fireEvent } from '@testing-library/react';
import ErrorState from '../ErrorState';

describe('test ErrorState', () => {
  it('renders error message', () => {
    render(<ErrorState />);
    expect(screen.getByText('No data fetched')).toBeInTheDocument();
  });
  it('renders retry button', async () => {
    const retry = jest.fn();
    render(<ErrorState retry={retry} />);
    const button = screen.getByText('Retry');
    expect(button).toBeInTheDocument();
    await fireEvent.click(button);
    expect(retry).toBeCalled();
  });
});
